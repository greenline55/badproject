export class User {
    id?: string;
    name: string;
    address: string;
    phone: string;
    description: string;
    userType: UserTypeEnum;
}

 export class Product {
    id?: string;
    name: string;
    vendor: string;
    price: number;
    unit: number;
}

export enum UserTypeEnum { 
    none = 0,
    supplier = 1,
    retailer = 2,
}