import { Injectable } from '@angular/core';

@Injectable()
export class PieChartService {
  public getDaySchedule() {
    return [
      {
        key: 'Молоко',
        hours: 9,
      },
      {
        key: 'Морковь',
        hours: 3,
      },
      {
        key: 'Кортошка',
        hours: 3,
      },
      {
        key: 'Мука',
        hours: 3,
      },
      {
        key: 'Огурцы',
        hours: 6,
      },
    ];
  }
}
