import { Injectable } from '@angular/core';

@Injectable()
export class TodoListService {
  public getItems(): object[] {
    return [
      {
        title: 'Найти  поставщиков молока',
        id: 1651644545,
        completed: false,
      },
      {
        title: 'Повысить продажи',
        id: 1651646545,
        completed: false,
      },
      {
        title: 'Улучшить показатели по продажам',
        id: 5451646545,
        completed: true,
      },
      {
        title: 'Найти  поставщиков моркови',
        id: 5428646545,
        completed: false,
      },
    ];
  }
}
