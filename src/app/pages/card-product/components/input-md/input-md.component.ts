import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-input-md',
  templateUrl: './input-md.component.html',
  styleUrls: ['./input-md.component.scss']
})
export class InputMdComponent {

  constructor() { 
    this.rand = this.getRand(1, 1000000);
  }
  
  rand: string;

  private getRand(min, max) {
    return Math.random() * (max - min) + min;
  }

  @Input() value: string;
  @Input() title: string;

}
