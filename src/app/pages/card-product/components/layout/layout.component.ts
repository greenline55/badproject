import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IProduct } from 'app/pages/listRetailer/components/list-products-retailer/list-products-retailer.component';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {
  
  /** Для отображения определенного продукта*/
  _id: number;
  /** Для запрета изменять */
  _isView: number;

  id: number;
  cost: number;
  unit: string;
  value: number;
  title: string;
  img: string;
  description: string;

  product: IProduct;

  constructor(private activateRoute: ActivatedRoute) {
    this._id = activateRoute.snapshot.queryParams.id ? +activateRoute.snapshot.queryParams.id : undefined;
    this._isView = activateRoute.snapshot.queryParams.v ? +activateRoute.snapshot.queryParams.v : undefined;
    console.warn("Тут нужно получтить информацию о продукте если есть id и сохранить это в this.product");
  }

  pageTitle: string;

  ngOnInit() {
    this.pageTitle = this.id ? "Редактирование продукта" : "Созание продукта";

    //if(!this.id)
    this.product = {
      id: 1,
      cost: 12,
      unit: "кг",
      value: 40,
      title: "Груша",
      img: "http://greenmart.com.ua/wp-content/uploads/2017/10/Grusha-Nikolay-Kryuger.jpg",
      coord: { x: 52.32, y: 72.34 },
      description: "Вкусный груша, клянусь кое-чем"
    }

    this.id = this.product.id;
    this.cost = this.product.cost;
    this.unit = this.product.unit;
    this.value = this.product.value;
    this.title = this.product.title;
    this.img = this.product.img;
    this.description = this.product.description;
  }

  onSave() {

    this.product.cost = this.cost;
    this.product.unit = this.unit;
    this.product.value = this.value;
    this.product.title = this.title;
    this.product.img = this.img;
    this.product.description = this.description;

    debugger
    this.product

    if (this.id)
      console.warn("написать сервис на сохранение", this.product)
    else
      console.warn("написать сервис на добавление", this.product)


  }
}
