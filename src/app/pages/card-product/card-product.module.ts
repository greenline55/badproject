import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './components/layout/layout.component';
import { FormsModule } from '@angular/forms';
import { ThemeModule } from 'theme';
import { MaterialAngularSelectModule } from 'material-angular-select';
import { InputMdComponent } from './components/input-md/input-md.component';

@NgModule({
  declarations: [LayoutComponent, InputMdComponent],
  imports: [
    CommonModule,
    FormsModule,
    ThemeModule,
    MaterialAngularSelectModule
  ]
})
export class CardProductModule { }
