import { Component, HostBinding } from '@angular/core';
import { UserService } from 'app/services/user.service';
import { User,UserTypeEnum } from 'app/models/models';

export interface IOrganisation {
  id?: number;
  name: string;
  userType: "1" | "2";
  phone: string;
  skype: string;
  mail: string;
  address: string;
  coordX: string;
  coordY: string;
  description: string;
}


@Component({
  selector: 'app-employer-form',
  styleUrls: ['./employer-form.component.scss'],
  templateUrl: 'employer-form.component.html',
})
export class EmployerFormComponent {
  @HostBinding('class.employer-form') private readonly employerForm = true;
  public qualifications = ['Young Padawan', 'Junior', 'Middle', 'Senior'];

  constructor(private userService:UserService){}

  object:IOrganisation =<IOrganisation> {};
  


  registration(){

    
    this.object.userType = "2";
    this.userService.saveUser(this.object).subscribe(res=>{
      console.log(res);
    })
  }

  onSelectMap(x: { x: number, y: number }) {
    this.object.coordX = x.x.toString();
    this.object.coordY = x.y.toString();
  }

  geoposition = { x: 55, y: 73.37 };

}
