import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule as NgFormsModule } from '@angular/forms';
import { MaterialAngularSelectModule } from 'material-angular-select';

import { ThemeModule } from 'theme';

import { EmployerFormComponent } from './employer-form';
import { FormsComponent } from './forms.component';
import { MapSelectorComponent } from 'app/components/map-selector/map-selector.component';
import { ListRetailerModule } from '../listRetailer/ListRetailer.module';

@NgModule({
  imports: [
    CommonModule,
    ThemeModule,
    NgFormsModule,
    MaterialAngularSelectModule,
    ListRetailerModule
  ],
  declarations: [
    FormsComponent,
    EmployerFormComponent,
    MapSelectorComponent
  ],
  providers: [],
})
export class FormsModule { }
