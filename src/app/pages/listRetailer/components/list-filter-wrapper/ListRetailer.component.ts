import { Component, ChangeDetectorRef, Input, OnInit } from '@angular/core';
import { UpgradableComponent } from 'theme/components/upgradable';
import { IGeoCoordinates } from '../filter-map/filter-map.component';

@Component({
  selector: 'list-retailer-component',
  styleUrls: ['./ListRetailer.component.scss'],
  templateUrl: './ListRetailer.component.html',
})
export class ListRetailerComponent extends UpgradableComponent implements OnInit{

  constructor(private cdr: ChangeDetectorRef) { super() }

  @Input() filters: { key: string, title: string }[];
  @Input() urlItem: string;
  @Input() dataList: any[]
  @Input() pageTitle: string;
  @Input() hideFilters: boolean;


  ngOnInit(){
    this.dataListFilters = this.dataList;
  }

  dataListFilters: any[];

  onFilterChanged(filters: { key: string, val: string }[]) {
    this.dataListFilters = this.dataList;
    filters.forEach(x => {
      this.dataListFilters = this.dataListFilters.filter(y => (y[x.key] + "").indexOf(x.val) !== -1);
    });
  }



  mapToggleHide: boolean = true;
  filtersToggleHide: boolean = true;

  mapCenter: IGeoCoordinates = { coord: { x: 55.73, y: 37.75 } };
  mapChangeFilter(x: IGeoCoordinates[]) {
    this.dataListFilters = x;
    this.cdr.detectChanges();
  }

}
