import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-filter-retail',
  templateUrl: './filter-retail.component.html',
  styleUrls: ['./filter-retail.component.scss']
})
export class FilterRetailComponent  {

  @Output()
  onFilterChanged =  new EventEmitter<{key: string, val: string}[]>();

  @Input()
  filters: [{key: string, title: string}] = <any>[];

  private filterList: {key: string, val: string}[]  = <any>[];

  filterChanged(key: string, val: string){
    let param = this.filterList.find(x=> x.key === key);
    if(param)
      param.val = val;
    else
      this.filterList.push({key: key, val: val});
    this.onFilterChanged.emit(this.filterList);
  }
}
