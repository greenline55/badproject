import { Component, OnInit, Input } from '@angular/core';
import { IProduct } from '../list-products-retailer/list-products-retailer.component';

@Component({
  selector: 'app-list-products-seller',
  templateUrl: './list-products-seller.component.html',
  styleUrls: ['./list-products-seller.component.scss']
})
export class ListProductsSellerComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    if(this.idSeller){
      console.warn("пролучаем список товаров для продавца ")
    }
    else{
      console.warn("для всех продавцов в системе")
    }
  }


  @Input() idSeller: number;


  dataList: IProduct[] = [
    { id: 1, cost: 42, unit: "кг", value: 700, title: "Картошка", img: "https://images.aif.ru/000/132/2e92593e0fed9cef29c4c8e8b05db742.jpg", coord: { x: 55.73, y: 37.75 }, description: "Вкусненькая очень, real talk" },
    { id: 2, cost: 45, unit: "кг", value: 450, title: "Картошка", img: "https://images.aif.ru/000/132/2e92593e0fed9cef29c4c8e8b05db742.jpg", coord: { x: 55.10, y: 37.45 } },
    { id: 3, cost: 65, unit: "л", value: 220, title: "Молоко", img: "https://shkolazhizni.ru/img/content/i189/189932_big.jpg", coord: { x: 55.25, y: 37.35 }, description: "Мягкая, пропадет через 2 дня, но люди возьмут"},
    { id: 4, cost: 83, unit: "л", value: 30, title: "Молоко", img: "https://shkolazhizni.ru/img/content/i189/189932_big.jpg", coord: { x: 55.25, y: 67.35 } },
  ]

  pageTitle: string = "Список продаваемых товаров";

  filters: { key: string, title: string }[] = [
    { key: "title", title: "Название продукта" },
    { key: "id", title: "id" }
  ];
  urlItem = '/app/task-product/';
  
}
