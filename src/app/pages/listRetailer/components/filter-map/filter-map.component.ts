import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

export interface IYaMapCoords {
  type: 'Point',
  coordinates: number[]
}

export interface IYaMap {
  geoQuery: (x: IYaMapCoords[]) => any;
  Map: any;
  ready: (x: () => void) => void;
  util: { Dragger: (x: any) => void }
}

declare var ymaps: IYaMap;

export interface IGeoCoordinates {
  coord: {
    x: number,
    y: number
  }
}

@Component({
  selector: 'app-filter-map',
  templateUrl: './filter-map.component.html',
  styleUrls: ['./filter-map.component.scss']
})
export class FilterMapComponent implements OnInit {

  constructor() { }

  @Input() objects: IGeoCoordinates[];
  objectsCopy: IGeoCoordinates[];
  @Input() center: IGeoCoordinates;
  @Output() onChange: EventEmitter<IGeoCoordinates[]> = new EventEmitter()

  onChangePosition = (x: { _objects: { geometry: { _coordinates: number[] } }[] }) => {
    let coords: { x: number, y: number }[] = [];
    x._objects.forEach(y => {
      coords.push({ x: y.geometry._coordinates[0], y: y.geometry._coordinates[1] });
    });
    if (coords.length === 0)
      return;

    let filters = [];
    this.objectsCopy.forEach(x => {
      coords.forEach(y => {
        if (x.coord.x === y.x && x.coord.y === y.y)
          filters.push(x);
      })
    });

    this.onChange.emit(filters);
  }

  allCoords: IYaMapCoords[] = [];

  ngOnInit() {
    this.objectsCopy = JSON.parse(JSON.stringify(this.objects))
    this.allCoords = this.objectsCopy.map(x => <IYaMapCoords>{ coordinates: [x.coord.x, x.coord.y], type: "Point" })

    let init = () => {
      let myMap = new ymaps.Map("map", {
        center: [this.center.coord.x, this.center.coord.y],
        zoom: 8
      }, {
          searchControlProvider: 'yandex#search'
        });

      let objects = ymaps.geoQuery(this.allCoords);

      objects.searchInside(myMap).addToMap(myMap);

      myMap.events.add('boundschange', () => {
        var visibleObjects = objects.searchInside(myMap).addToMap(myMap);
        this.onChangePosition(visibleObjects);
        objects.remove(visibleObjects).removeFromMap(myMap);
      });
    }

    ymaps.ready(init);
  }

}
