import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ListRetailerComponent } from './components/list-filter-wrapper/ListRetailer.component';
import { FilterRetailComponent } from './components/filter-retail/filter-retail.component';
import { ListComponent } from './components/list/list.component';
import { RouterModule } from '@angular/router';
import { FilterMapComponent } from './components/filter-map/filter-map.component';
import { ListProductsSellerComponent } from './components/list-products-seller/list-products-seller.component';
import { ListProductsRetailerComponent } from './components/list-products-retailer/list-products-retailer.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [
    ListRetailerComponent,
    FilterRetailComponent,
    ListComponent,
    FilterMapComponent,
    ListProductsSellerComponent,
    ListProductsRetailerComponent
  ],
  exports: [
    ListProductsSellerComponent,
    ListProductsRetailerComponent
  ]
})
export class ListRetailerModule {}
