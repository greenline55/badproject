import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from 'app/models/models';
import { IOrganisation } from 'app/pages/forms/employer-form/employer-form.component';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  urlSite = 'http://www.greenline.somee.com/APIService.svc/';

  constructor(private http: HttpClient) { }

  saveUser(obj:IOrganisation): Observable<string> {
    return this.http.post<string>(this.urlSite+"SaveUser",obj);
  }

  getUser(){
    return this.http.get<IOrganisation>(this.urlSite+"GatUser");
  }


}
