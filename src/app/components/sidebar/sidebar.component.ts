import { Component, Input } from '@angular/core';

import { SidebarComponent as BaseSidebarComponent } from 'theme/components/sidebar';

@Component({
  selector: 'app-sidebar',
  styleUrls: ['../../../theme/components/sidebar/sidebar.component.scss', './sidebar.component.scss'],
  templateUrl: '../../../theme/components/sidebar/sidebar.component.html',
})
export class SidebarComponent extends BaseSidebarComponent {
  public title = 'GreenLine';
  public menu = [
    { name: 'На закупку', link: '/app/list-retailer', icon: 'dashboard' },
    { name: 'На продажу', link: '/app/list-seller', icon: 'dashboard' },
  
    // {
    //   name: 'UI',
    //   children: [
    //     ...[
    //       'buttons',
    //       'cards',
    //       'colors',
    //       'forms',
    //       'icons',
    //       'typography',
    //       'tables',
    //     ].map(ui => ({
    //       name: ui[0].toUpperCase() + ui.slice(1),
    //       link: `/ui/${ui}`,
    //     })),
    //     {
    //       name: 'Right sidebar',
    //       link: '/ui/right-sidebar',
    //     },
    //   ],
    //   icon: 'view_comfy',
    // },
    // { name: 'Components', link: '/app/components', icon: 'developer_board' },
    { name: 'Аккаунт', link: '/app/forms', icon: 'person' },
    { name: 'Статистика', link: '/app/dashboard', icon: 'view_quilt' },
    // {
    //   name: 'Распложение', icon: 'map', children: [
    //   { name: 'Поставщики', link: '/maps/simple' },
    //   { name: 'Магазины', link: '/maps/advanced' },
    //   ],
    // },
    // { name: 'Отчеты', link: '/app/charts', icon: 'multiline_chart' },
    // {
    //   name: 'Pages', children: [
    //   { name: 'Sign in', link: '/pages/login' },
    //   { name: 'Sign up', link: '/pages/sign-up' },
    //   { name: 'Forgot password', link: '/pages/forgot-password' },
    //   { name: '404', link: '/pages/error' },
    //   ],
    //   icon: 'pages',
    // },
  ];
}
