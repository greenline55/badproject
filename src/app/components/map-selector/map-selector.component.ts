import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { IYaMap } from 'app/pages/listRetailer/components/filter-map/filter-map.component';

declare var ymaps: IYaMap;

@Component({
  selector: 'app-map-selector',
  templateUrl: './map-selector.component.html',
  styleUrls: ['./map-selector.component.scss']
})
export class MapSelectorComponent implements OnInit {

  constructor() { }

  @Input() center: {x: number, y: number};
  @Output() onSelect: EventEmitter<{ x: number, y: number }> = new EventEmitter();

  onSetCoord = (x: number, y: number) => {
    this.onSelect.emit({ x: x, y: y })
  }

  ngOnInit() {
    ymaps.ready(this.init);
  }


  init = () => {
    var map = new ymaps.Map('map', {
      center: [this.center.x, this.center.y],
      zoom: 10
    }, {
        searchControlProvider: 'yandex#search'
      }),
      markerElement: HTMLDivElement = document.querySelector('#marker'),
      dragger = new ymaps.util.Dragger({
        autoStartElement: markerElement
      }),
      markerOffset,
      markerPosition;

    let applyDelta = (event) => {
      var delta = event.get('delta');
      markerPosition[0] += delta[0];
      markerPosition[1] += delta[1];
      applyMarkerPosition();
    }

    let applyMarkerPosition = () => {
      markerElement.style.left = markerPosition[0] + "px";
      markerElement.style.top = markerPosition[1] + "px";
    }

    let containsPoint = (bounds, point) => {
      return point[0] >= bounds[0][0] && point[0] <= bounds[1][0] &&
        point[1] >= bounds[0][1] && point[1] <= bounds[1][1];
    }

    let getTileCoordinate = (coords, zoom, tileSize) => {
      return [
        Math.floor(coords[0] * zoom / tileSize),
        Math.floor(coords[1] * zoom / tileSize)
      ];
    }




    let onDraggerStart = (event) => {
      let position: number[] = event.get('position');
      markerOffset = [
        position[0] - markerElement.offsetLeft,
        position[1] - markerElement.offsetTop
      ];
      markerPosition = [
        position[0] - markerOffset[0],
        position[1] - markerOffset[1]
      ];

      applyMarkerPosition();
    }

    let onDraggerMove = (event) => {
      applyDelta(event);
    }

    let onDraggerEnd = (event) => {
      applyDelta(event);
      markerPosition[0] += markerOffset[0];
      markerPosition[1] += markerOffset[1];
      var markerGlobalPosition = map.converter.pageToGlobal(markerPosition),
        mapGlobalPixelCenter = map.getGlobalPixelCenter(),
        mapContainerSize = map.container.getSize(),
        mapContainerHalfSize = [mapContainerSize[0] / 2, mapContainerSize[1] / 2],
        mapGlobalPixelBounds = [
          [mapGlobalPixelCenter[0] - mapContainerHalfSize[0], mapGlobalPixelCenter[1] - mapContainerHalfSize[1]],
          [mapGlobalPixelCenter[0] + mapContainerHalfSize[0], mapGlobalPixelCenter[1] + mapContainerHalfSize[1]]
        ];
      if (containsPoint(mapGlobalPixelBounds, markerGlobalPosition)) {
        var geoPosition: number[] = map.options.get('projection').fromGlobalPixels(markerGlobalPosition, map.getZoom());
        this.onSetCoord(+geoPosition[0].toFixed(2), +geoPosition[1].toFixed(2));
      }
    }

    dragger.events
      .add('start', onDraggerStart)
      .add('move', onDraggerMove)
      .add('stop', onDraggerEnd);

  }

}
