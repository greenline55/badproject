> BadProject 

[![pipeline status](https://gitlab.com/greenline55/badproject/badges/master/pipeline.svg)](https://gitlab.com/greenline55/badproject/commits/master)

Must ToDo
- [x] Страница настройки аккаунта
- [ ] Страница настройки аккаунта - Сервис
- [x] Страница отображения Доступных товаров
- [ ] Страница отображения Доступных товаров - Сервис
- [x] Страница отображения Доступных Предложений о покупке от Магазинов
- [ ] Страница отображения Доступных Предложений о покупке от Магазинов - Сервис
- [ ] Страница Магазина
- [ ] Страница Поставщика
- [ ] Оповещение о заказе Поставщику 
- [ ] Оповещение о заказе Магазину 
- [ ] страница создания продукта 
- [-] страница редактирования продукта 


**board** - https://gitlab.com/groups/greenline55/-/boards

**for run front** - npm i && ng serve

**stage server to view** - https://dev.lab-27.ru/

**for install buildAgent** - run powershell: Set-ExecutionPolicy RemoteSigned

**angular template** - https://github.com/CreativeIT/material-angular-dashboard

**Python 2.7** - https://www.python.org/downloads/release/python-2716/

**node-sass** - npm i node-sass

**if proxy** npm config edit -> strict-ssl=false; HTTP_PROXY=http://example.com:1234


getUser => IOrganisation
getAllProducts => IProduct[]
getProducts(idOrganisation) => IProduct[]


`IOrganisation {
  id: number;
  name: string;
  type: 1 | 2;
  phone: string;
  skype: string;
  mail: string;
  address: string;
  coord: {
    x: number,
    y: number
  };
  description: string;
}

IProduct {
  id: number;
  cost: number
  unit: string, 
  value: number, 
  title: string, 
  img: string, 
  coord: { x: number, y: number }, 
  description?: string
}
`

