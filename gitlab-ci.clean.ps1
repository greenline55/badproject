﻿
param($user, $pass)


function DeleteFtpFolder($url, $credentials)
{
    $listRequest = [Net.WebRequest]::Create($url)
    $listRequest.Method = [System.Net.WebRequestMethods+Ftp]::ListDirectoryDetails
    $listRequest.Credentials = $credentials

    $lines = New-Object System.Collections.ArrayList

    $listResponse = $listRequest.GetResponse()
    $listStream = $listResponse.GetResponseStream()
    $listReader = New-Object System.IO.StreamReader($listStream)
    while (!$listReader.EndOfStream)
    {
        $line = $listReader.ReadLine()
        $lines.Add($line) | Out-Null
    }
    $listReader.Dispose()
    $listStream.Dispose()
    $listResponse.Dispose()

    foreach ($line in $lines)
    {
        $tokens = $line.Split(" ", 9, [StringSplitOptions]::RemoveEmptyEntries)
        $name = $tokens[8]
        $permissions = $tokens[0]

        $fileUrl = ($url + $name)

        if ($permissions[0] -eq 'd')
        {
            DeleteFtpFolder ($fileUrl + "/") $credentials
        }
        else
        {
            Write-Host "Deleting file $name"
            $deleteRequest = [Net.WebRequest]::Create($fileUrl)
            $deleteRequest.Credentials = $credentials
            $deleteRequest.Method = [System.Net.WebRequestMethods+Ftp]::DeleteFile
            $deleteRequest.GetResponse() | Out-Null
        }
    }

    Write-Host "Deleting folder"
    $deleteRequest = [Net.WebRequest]::Create($url)
    $deleteRequest.Credentials = $credentials
    $deleteRequest.Method = [System.Net.WebRequestMethods+Ftp]::RemoveDirectory
    $deleteRequest.GetResponse() | Out-Null
}



try
{
    $url = "ftp://hegel.timeweb.ru/public_html/";
    $credentials = New-Object System.Net.NetworkCredential($user, $pass)
    DeleteFtpFolder $url $credentials

}
catch [Net.WebException]
{
}


$webclient = New-Object System.Net.WebClient
$webclient.Credentials = New-Object System.Net.NetworkCredential($user,$pass) 
try
{
    $makeDirectory = [System.Net.WebRequest]::Create("ftp://hegel.timeweb.ru/public_html");
    $makeDirectory.Credentials = New-Object System.Net.NetworkCredential($user,$pass)
    $makeDirectory.Method = [System.Net.WebRequestMethods+FTP]::MakeDirectory;
    $makeDirectory.GetResponse();
}
catch [Net.WebException]
{
    Write-Host $item.Name Directory may be already exists.
}
