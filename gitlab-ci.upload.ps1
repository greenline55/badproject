﻿param($user, $pass)

$ftp_uri = "ftp://hegel.timeweb.ru/"
$pathCurrent = Get-Location
$path= "$pathCurrent\dist"
$webclient = New-Object System.Net.WebClient
$webclient.Credentials = New-Object System.Net.NetworkCredential($user,$pass) 
$files =  Get-ChildItem -recurse $path
 foreach($item in Get-ChildItem -recurse $path)
{
  $relpath = [system.io.path]::GetFullPath($item.FullName).SubString([system.io.path]::GetFullPath($path).Length + 1)
        if ($item.Attributes -eq "Directory" -or $item.Attributes -eq "Directory, NotContentIndexed")
         {
            try
             {
                Write-Host Creating $item.Name
                $makeDirectory = [System.Net.WebRequest]::Create($ftp_uri +"public_html/"+$relpath);
                $makeDirectory.Credentials = New-Object System.Net.NetworkCredential($user,$pass)
                $makeDirectory.Method = [System.Net.WebRequestMethods+FTP]::MakeDirectory;
                $makeDirectory.GetResponse();
            }
            catch [Net.WebException]
             {
                Write-Host $item.Name Directory may be already exists.
            }
            continue;
        }
        "Uploading $item to :  $relpath"
        $uri = New-Object System.Uri($ftp_uri+"public_html/"+$relpath)
        $webclient.UploadFile($uri, $item.FullName)
    }